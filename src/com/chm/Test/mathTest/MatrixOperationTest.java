package com.chm.Test.mathTest;
/**
 * author:Cheng Huiming
 * date:2018.1.18 15:11 +0800
 * description:matrix operation and determinant operation
 */

import com.chm.math.determinant.Determinant;
import com.chm.math.matrix.Matrix;
import java.util.Scanner;
import static com.chm.math.matrix.Matrix.*;


//public class MatrixOperationTest {
    /**
     * Main method
     */
    /**public static void main(String[] args) throws Exception {
        //create a scanner place
        Scanner input = new Scanner(System.in);
        System.out.println("Please input the row and column of a Matrix：(For example:3 3)");
        //input matrix row and column
        int row = input.nextInt();
        int column = input.nextInt();
        //define m1
        Matrix m1 = creatMatrix (row,column);
        System.out.println("The matrix is:");
        System.out.println(m1.toString());
        System.out.println("Please input the row and column of a Matrix：(For example:3 3)");
        row = input.nextInt();
        column = input.nextInt();
        //define m2
        Matrix m2 = creatMatrix (row,column);
        System.out.println("The matrix is:");
        System.out.println(m2.toString());
        //define m3
        Matrix m3 = Add (m1,m2);
        //define m4
        Matrix m4 = Minus(m1,m2);
        //define m5
        Matrix m5 = Multiplication(m1,m2);
        //define m7
        Matrix m7= m1.Transpose();
        System.out.println("The transpose matrix is:\n"+m7.Transpose());
        //define m8
        Matrix m8= m2.Transpose();
        System.out.println("The transpose matrix is:\n"+m8.Transpose());
        //define m9
        Determinant m9 = new Determinant(m1);
        System.out.println("The determinant1 determ is:"+m9.getDeterm());
        //define m10
        Determinant m10 = new Determinant(m2);
        System.out.println("The determinant2 determ is:"+m10.getDeterm());
        //define m11
        Matrix m11 = m1.getAlgebraicComplementMinorMatrix();
        System.out.println("The matrix1 minor matrix is:");
        System.out.println(m11.toString());
        //define m12
        Matrix m12 =m2.getAlgebraicComplementMinorMatrix();
        System.out.println("The matrix2 minor matrix is:");
        System.out.println(m12.toString());
        //define m13
        Matrix m13 = m1.invertibleMatrix();
        System.out.println("The matrix1 inverse matrix is:");
        System.out.println(m13.toString());
        //define m14
        Matrix m14 = m2.invertibleMatrix();
        System.out.println("The matrix2 inverse matrix is:");
        System.out.println(m14.toString());
        //define m15
        Matrix m15=m1.unit();
        System.out.println("The unit matrix is:");
        System.out.println(m15.toString());

    }
*/
