package com.chm.Test.mathTest;
import com.chm.math.matrix.Matrix;
import java.util.Scanner;
import static com.chm.math.matrix.Matrix.*;
import com.chm.math.leastsquare.*;


public class LeastSquareTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("请输入数据点的个数");
        int n = input.nextInt();
        Matrix matrix = new Matrix(n,2);
        for (int i = 0; i < n; i++) {
            System.out.println("请输入x列表"+i+"位置的数值");
            double x= input.nextDouble();
            matrix.set(i,0,x);
            System.out.println("请输入y列表"+i+"位置的数值");
            double y= input.nextDouble();
            matrix.set(i,1,y);
        }
        System.out.println("请输入最小二乘拟合的阶数");
        int k = input.nextInt();
        System.out.println("数据表是");
        System.out.println(matrix.toString());
        LeastSquare dataList = new LeastSquare(matrix);
        Matrix cofficientMatrix = dataList.CofficientMatrix(k);
        Matrix bMatrix = dataList.BMatrix(k);
        Matrix amatrix = Multiplication(bMatrix,cofficientMatrix.invertiblematrix());
        System.out.println("最小二乘拟合方程为：");
        System.out.print(amatrix.get(0,0)+"+");
        for (int i = 0; i <k ; i++) {
            System.out.print(amatrix.get(i,0)+"*x^"+i+"+");
        }
        System.out.print(amatrix.get(n,0)+"*x^"+k);
        System.out.println("=0");

    }
}
