package com.chm.math.leastsquare;
import com.chm.math.matrix.Matrix;


public class LeastSquare {
   public double[][] DataList;
   public int row;
   public double[] XDataList;
   public double[] YDataList;

    /**
     * @description: 初始化数据列表
     * @param matrix 输入的矩阵
     */
   public LeastSquare(Matrix matrix){
       this.row = matrix.getRow();
       this.DataList =  new double[this.row][2];
       this.XDataList = new double[this.row];
       this.YDataList = new double[this.row];
       for (int i = 0; i <matrix.getRow() ; i++) {
           for (int j = 0; j <2 ; j++) {
               this.DataList[i][j] = matrix.get(i,j);
           }
       }
       for (int i = 0; i <matrix.getRow() ; i++) {
           this.XDataList[i] = matrix.get(i,0);
       }
       for (int i = 0; i <matrix.getRow() ; i++) {
           this.YDataList[i]=matrix.get(i,1);
       }
   }

    /**
     * @description: 系数矩阵
     * @param n 拟合函数阶数
     * @return 系数矩阵
     */
   public Matrix CofficientMatrix(int n){
       Matrix CofficientMatrix = new Matrix(n+1,n+1);
       for (int i = 0; i <CofficientMatrix.getRow(); i++) {
           for (int j = 0; j <CofficientMatrix.getColumn() ; j++) {
               double sum = 0;
               for (int k = 0; k <this.row ; k++) {
                   sum = sum +Math.pow(this.XDataList[k],i+j);
               }
                CofficientMatrix.set(i,j,sum);
           }
       }
       return CofficientMatrix;
   }

    /**
     * @description: B矩阵
     * @param n 阶数
     * @return B矩阵
     */
   public Matrix BMatrix(int n){
       Matrix BMatrix = new Matrix(n+1,1);
       for (int i = 0; i <BMatrix.getRow() ; i++) {
           double sum=0;
           for (int j = 0; j <BMatrix.getRow() ; j++) {
               sum = sum + Math.pow(this.XDataList[j],i)*this.YDataList[j];
           }
           BMatrix.set(i,0,sum);
       }
       return BMatrix;
   }
}
