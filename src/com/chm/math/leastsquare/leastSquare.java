package com.chm.math.leastsquare;
import com.chm.math.matrix.Matrix;

public class leastSquare {
    private double[] x;
    private double[] y;
    private int n;
    private double [] coefficient;
    private double [] weight;

    /**
     *
     * @param x
     * @param y
     * @param n
     */
    public leastSquare(double[]x,double[]y,int n){
        if(x==null||y==null||x.length<2||x.length!=y.length||n<2){throw new IllegalArgumentException("Can not fit!");}
        this.x=x;
        this.y = y;
        this.n = n;
        weight = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            weight[i] = 1;
        }
        compute();
    }

    /**
     *
     * @param x
     * @param y
     * @param weight
     * @param n
     */
    public leastSquare(double[]x,double[]y,double[] weight, int n){
        if(x==null||y==null||weight==null||x.length<2||x.length!=y.length||x.length!=weight.length||n<2){throw new IllegalArgumentException("Can not fit!");}
        this.x=x;
        this.y = y;
        this.n = n;
        this.weight=weight;
        compute();
    }

    /**
     *
     * @return
     */
    public double[] getCoefficient() {
        return coefficient;
    }

    /**
     *
     * @param x
     * @return
     */
    public double fit(double x) {
        if (coefficient == null) {
            return 0;
        }
        double sum = 0;
        for (int i = 0; i < coefficient.length; i++) {
            sum += Math.pow(x, i) * coefficient[i];
        }
        return sum;
    }

    /**
     *
     * @param y
     * @return
     */
    public double solve(double y) {
        return solve(y);
    }

    /**
     *
     * @param y
     * @param startX
     * @return
     */
    public double solve(double y, double startX) {
        final double EPS = 0.0000001d;
        if (coefficient == null) {
            return 0;
        }
        double x1 = 0.0d;
        double x2 = startX;
        do {
            x1 = x2;
            x2 = x1 - (fit(x1) - y) / calcReciprocal(x1);
        } while (Math.abs((x1 - x2)) > EPS);
        return x2;
    }

    /**
     *
     * @param x
     * @return
     */
    private double calcReciprocal(double x) {
        if (coefficient == null) {
            return 0;
        }
        double sum = 0;
        for (int i = 1; i < coefficient.length; i++) {
            sum += i * Math.pow(x, i - 1) * coefficient[i];
        }
        return sum;
    }

    /**
     *
     */
    private void compute() {
        if (x == null || y == null || x.length <= 1 || x.length != y.length
                || x.length < n || n < 2) {
            return;
        }
        double[] s = new double[(n - 1) * 2 + 1];
        for (int i = 0; i < s.length; i++) {
            for (int j = 0; j < x.length; j++) {
                s[i] += Math.pow(x[j], i) * weight[j];
            }
        }
        double[] b = new double[n];
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < x.length; j++) {
                b[i] += Math.pow(x[j], i) * y[j] * weight[j];
            }
        }
        double[][] a = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = s[i + j];
            }
        }
        //coefficient = calcLinearEquation(a, b);
    }

}
