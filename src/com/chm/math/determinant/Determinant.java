package com.chm.math.determinant;

/**
 * author:Cheng Huiming
 * date:2018.1.18 15:11 +0800
 * description:matrix function
 */
import com.chm.math.matrix.Matrix;

public class Determinant {
    private double[][] determinantList;
    private int order;
    private double determ;

    /**
     * @description: construct two-dimensional array
     * @param order the order is subscript
     */
    public Determinant(int order) {
        this.order = order;
        this.determinantList = new double[this.order][this.order];
    }

    /**
     * @description: get two-dimensional array
     * @param row the row is array subscript
     * @param column the column is array subscript
     * @return the two-dimensional array element
     */
    public double get(int row, int column) {
        return this.determinantList[row][column];
    }

    /**
     * @description: set two-dimensional array
     * @param row the row is array subscript
     * @param column the column is array subscript
     * @param value the value is array element
     */
    public void set(int row, int column, double value) {
        this.determinantList[row][column] = value;
    }

    /**
     * @description: get array order
     * @return order
     */
    public int getOder() {
        return order;
    }

    /**
     *@description: according to matrix get determinant
     * @param matrix the matrix is matrix
     * @throws Exception
     */
    public Determinant(Matrix matrix) {
        if (matrix.getRow() != matrix.getColumn()) {
            System.out.println("矩阵不是方阵，不能进行行列式运算");
        }
       else{
            this.order = matrix.getRow();
            this.determinantList = new double[this.order][this.order];
            for (int i = 0; i < this.order; i++) {
                for (int j = 0; j < this.order; j++) {
                    this.determinantList[i][j] = matrix.get(i, j);
                }
            }
        }
    }

    /**
     * @description: get Algebraic Complement Minor
     * @param row the row is matrix row
     * @param column the column is matrix column
     * @return Algebraic Complement Minor
     * @throws Exception
     */
    public  Determinant getAlgebraicComplementMinor(int row,int column) {
        Determinant resultDterminant = new Determinant(this.order - 1);
        if (row>this.order||column>this.order){
            System.out.println("超出计算范围");
        }
        else {
            for (int i = 0; i < this.order - 1; i++) {
                for (int j = 0; j < this.order - 1; j++) {
                    if (i < row && j < column) {
                        resultDterminant.set(i, j, this.get(i, j));
                    } else if (i >= row && j < column) {
                        resultDterminant.set(i, j, this.get(i + 1, j));
                    } else if (i < row && j >= column) {
                        resultDterminant.set(i, j, this.get(i, j + 1));
                    } else {
                        resultDterminant.set(i, j, this.get(i + 1, j + 1));
                    }
                }
            }
        }
        return resultDterminant;
    }

    /**
     * @description: get deter
     * @return det
     */
    public double getDeterm(){
        if (this.order==1){
            return this.get(0,0);
        }
        else if (this.order==2){
            return  this.get(0,0)*this.get(1,1)-this.get(0,1)*this.get(0,1);
        }
        else{
            for (int i = 0; i <this.order ; i++) {
                if (this.get(0,i)!=0){
                    this.determ += Math.pow(-1,i)*this.get(0,i)*this.getAlgebraicComplementMinor(0,i).getDeterm();
                }
            }
        }
        return determ;
    }
}


