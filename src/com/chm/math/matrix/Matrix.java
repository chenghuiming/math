package com.chm.math.matrix;
/**
 * author:Cheng Huiming
 * date:2018.1.18 15:11 +0800
 * description:matrix function
 */

import com.chm.math.determinant.Determinant;
import java.util.Scanner;

public class Matrix {
    private double[][] matrixArray;
    private int column;
    private int row;
    private Matrix transposeMatrix;
    private Matrix unitmatrix;

    /**
     * @description:  construct matrix
     * @param row the row is matrix row
     * @param column the column is matrix column
     */
    public Matrix(int row,int column){
        this.row=row;
        this.column=column;
        this.matrixArray=new double[row][column];
    }


    /**
     * @description: Set the value of the matrix element.
     * @param i the i is subscript
     * @param j the j is subscript
     * @param value the value is element
     */
    public void set(int i,int j,double value){
        this.matrixArray[i][j]=value;
    }

    /**
     * @description: get the value of the matrix element.
     * @param i the i is subscript
     * @param j the j is subscript
     * @return  matrix element
     */
    public double get(int i,int j){
        return this.matrixArray[i][j];
    }

    /**
     * @description: get the value of the matrix column.
     * @return matrix column
     */
    public int getColumn(){
        return this.column;
    }

    /**
     * @description: get the value of the matrix grow.
     * @return matrix grow
     */
    public int getRow(){
        return this.row;
    }

    /**
     * @description: design a matrix
     * @param matrix the matrix is matrix
     */
    public Matrix(Matrix matrix){
        this.column=matrix.column;
        this.row=matrix.row;
        this.matrixArray = new double[this.row][this.column];
    }

    /**
     * @description: create a unit matrix
     * @return unit matrix
     */
    public Matrix unit(){
        this.unitmatrix = new Matrix(this.column, this.row);
        if (column!=row){
            System.out.println("矩阵不是方阵无法求单位矩阵");
        }
        else {
            for (int i = 0; i < this.row; i++) {
                for (int j = 0; j < this.column; j++) {
                    if (i == j) {
                        this.matrixArray[i][j] = 1;
                    } else {
                        this.matrixArray[i][j] = 0;
                    }
                    this.unitmatrix.set(i, j, this.get(i, j));
                }
            }
            System.out.println("The unit matrix is:");
            System.out.println(unitmatrix.toString());
        }
        return unitmatrix;
    }

    /**
     * @description: matrix transpose
     * @return transpose matrix
     */
    public Matrix Transpose(Matrix matrix){
        this.transposeMatrix = new Matrix(matrix.column, matrix.row);
        for (int i = 0; i <matrix.row ; i++) {
            for (int j = 0; j <matrix.column ; j++) {
                this.transposeMatrix.set(i, j, this.get(j, i));
            }
        }
        return transposeMatrix;
    }

    /**
     * @description: matrix addition
     * @param m1 the m1 is matrix
     * @param m2 the m2 is matrix
     * @return matrix
     */
    public static Matrix Add(Matrix m1,Matrix m2) {
        Matrix resultaddMatrix=new Matrix(m1.row,m1.column);
        if (m1.row!=m2.row||m1.column!=m2.column) {
            System.out.println("两个矩阵大小不一致，不能相加");
        }
        else{
            for (int i = 0; i <m1.row ; i++) {
                for (int j = 0; j <m1.column ; j++) {
                    resultaddMatrix.set(i,j,m1.get(i,j)+m2.get(i,j));
                }
            }
            System.out.println("The matrix add is:");
            System.out.println(resultaddMatrix.toString());
        }
        return resultaddMatrix;
    }

    /**
     * @description: matrix subtraction
     * @param m1 the m1 is matrix
     * @param m2 the m2 is matrix
     * @return matrix
     */
    public static Matrix Minus(Matrix m1,Matrix m2){
        Matrix  resultminusMatrix=new Matrix(m1.row,m1.column);
        if (m1.row!=m2.row||m1.column!=m2.column) {
            System.out.println("两个矩阵大小不一致，不能相减");
        }
        else{
            for (int i = 0; i <m1.row ; i++) {
                for (int j = 0; j <m1.column ; j++) {
                    resultminusMatrix.set(i,j,m1.get(i,j)-m2.get(i,j));
                }
            }
            System.out.println("The matrix minus is:");
            System.out.println(resultminusMatrix.toString());
        }
        return resultminusMatrix;
    }

    /**
     * @description: matrix multiplication
     * @param m1 the m1 is matrix
     * @param m2 the m2 is matrix
     * @return matrix
     */
    public static Matrix Multiplication(Matrix m1, Matrix m2){
        Matrix resultmutlipMatrix=new Matrix(m1.row,m2.column);
        if(m1.column!=m2.row){
            System.out.println("两个矩阵对应关系不一致，不能相乘"); }
        else{
            for (int i = 0; i < m1.row; i++)
                for (int j = 0; j < m2.column; j++)
                    for (int k = 0; k < m1.column; k++)
                        resultmutlipMatrix.set(i,j,resultmutlipMatrix.get(i,j)+(m1.get(i, k) * m2.get(k, j)));
                        System.out.println("The matrix multiplication is:");
                        System.out.println(resultmutlipMatrix.toString());
        }
        return resultmutlipMatrix;
    }

    public Matrix multiconstant(double k,Matrix matrix){
        Matrix resultmutliMatrix = new Matrix(matrix.row, matrix.column);
        for (int i = 0; i < matrix.row; i++) {
            for (int j = 0; j < matrix.column; j++) {
                resultmutliMatrix.set(i, j, matrix.get(i, j) * k);
            }
        }
        return resultmutliMatrix;
    }


    /**
     * @description: get Algebraic Complement Minor Matrix
     * @return matrix
     */
    public  Matrix getAlgebraicComplementMinorMatrix(){
        Matrix resultMatrix=new Matrix(this.row,this.column);
        Determinant determinant = new Determinant(this);
        for (int i = 0; i <this.row ; i++) {
            for (int j = 0; j <this.column ; j++) {
                resultMatrix.set(i,j,determinant.getAlgebraicComplementMinor(i, j).getDeterm() * Math.pow(-1, i + j ));
            }
        }
        return resultMatrix;
    }

    /**
     *
     * @return
     */
     public double matrixNorm(){
         return new Determinant(this).getDeterm();
     }

    /**
     *
     * @return
     */
    public Matrix invertiblematrix()  {
        return multiconstant(1/matrixNorm(),getAlgebraicComplementMinorMatrix());
    }

    /**
     * @description: create a matrix
     * @param row the matrix row
     * @param column the matrix column
     * @return matrix
     */
    public static Matrix creatMatrix(int row,int column){
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter " + row + " row" + column + " column （row first）：");
        Matrix matrix = new Matrix(row,column);
        for (int i = 0; i <row ; i++) {
            for (int j = 0; j <column ; j++) {
                double vaule=input.nextDouble();
                matrix.set(i,j,vaule);
            }
        }
        return matrix;
    }

    /**
     * @description: output matrix
     * @return string
     */
    public String toString(){
        String resultsString = "";
        for (int i = 0; i < this.row; i++){
            for (int j = 0; j < this.column; j++){
                resultsString += (String.valueOf(this.get(i, j)) + "\t");
            }
            resultsString += "\n";
        }
        return resultsString;
    }
}
